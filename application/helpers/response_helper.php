<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Response_helper {
    public function __construct() {}

    // Return JSON Patter
    public function json($res, $code=200) {
        header('Content-Type: application/json');
        return json_encode(array(
            "data"  => $res,
            "code"  => $code
        ), JSON_PRETTY_PRINT);
    }
}