/* TABLES > MEDIAS */
CREATE TABLE medias (
    id INT(11) PRIMARY KEY AUTO_INCREMENT,
    label VARCHAR(255) NOT NULL,
    uri TEXT NOT NULL,
    file_name TEXT NOT NULL,
    created_at VARCHAR(20) NULL,
    updated_at VARCHAR(20) NULL
);

/* TABLES > USERS */
CREATE TABLE users (
    id INT(11) PRIMARY KEY AUTO_INCREMENT,
    username VARCHAR(255) NOT NULL,
    password VARCHAR(255) NOT NULL,
    full_name VARCHAR(255) NOT NULL,
    phone_number VARCHAR(15) NOT NULL,
    address TEXT NULL,
    face_id INT(11) DEFAULT 1,
    created_at VARCHAR(20) NULL,
    updated_at VARCHAR(20) NULL,
    parent_id INT(11) DEFAULT 0,
    token VARCHAR(255) NULL DEFAULT NULL,
    INDEX face_id_user_media (face_id),
    FOREIGN KEY (face_id)
        REFERENCES medias(id)
        ON DELETE CASCADE
);

/* TABLES > USERS > UTILITY */
CREATE TABLE users_utility (
    id INT(11) PRIMARY KEY AUTO_INCREMENT,
    user_type TINYINT(1) DEFAULT 0,
    balance BIGINT(20) DEFAULT 0,
    transaction BIGINT(20) DEFAULT 0,
    stuff BIGINT(20) DEFAULT 0,
    supplier BIGINT(20) DEFAULT 0,
    user_id INT(11),
    INDEX user_id_utility (user_id),
    FOREIGN KEY (user_id)
        REFERENCES users(id)
        ON DELETE CASCADE
);

/* TABLES > SUPPLIERS */
CREATE TABLE suppliers (
    id INT(11) PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(255) NOT NULL,
    address TEXT NOT NULL,
    description TEXT NOT NULL,
    banner_id INT(11) DEFAULT 2,
    face_id INT(11) DEFAULT 1,
    author_id INT(11),
    created_at VARCHAR(20) NULL,
    updated_at VARCHAR(20) NULL,
    INDEX banner_id_supplier (banner_id),
    FOREIGN KEY (banner_id)
        REFERENCES medias(id)
        ON DELETE CASCADE,
    INDEX face_id_supplier (face_id),
    FOREIGN KEY (face_id)
        REFERENCES medias(id)
        ON DELETE CASCADE,
    INDEX author_id_supplier (author_id),
    FOREIGN KEY (author_id)
        REFERENCES users(id)
        ON DELETE CASCADE
);

/* TABLES > SUPPLIERS > UTILITY */
CREATE TABLE suppliers_utility (
    id INT(11) PRIMARY KEY AUTO_INCREMENT,
    is_banned TINYINT(1) DEFAULT 0,
    balance BIGINT(20) DEFAULT 0,
    transaction BIGINT(20) DEFAULT 0,
    casier BIGINT(20) DEFAULT 0,
    stuff BIGINT(20) DEFAULT 0,
    supplier_id INT(11),
    INDEX supplier_id_utility (supplier_id),
    FOREIGN KEY (supplier_id)
        REFERENCES suppliers(id)
        ON DELETE CASCADE
);

/* TABLES > SUPPLIERS > STUFF SUPPLIERS */
CREATE TABLE suppliers_stuff (
    id INT(11) PRIMARY KEY AUTO_INCREMENT,
    label VARCHAR(255) NOT NULL,
    code VARCHAR(255) NOT NULL,
    description TEXT NOT NULL,
    qty BIGINT(20) DEFAULT 0,
    price_default BIGINT(20) DEFAULT 0,
    price_selling BIGINT(20) DEFAULT 0,
    expired_date VARCHAR(20) NULL,
    created_at VARCHAR(20) NULL,
    updated_at VARCHAR(20) NULL,
    supplier_id INT(11),
    INDEX supplier_id_stuff_supplier (supplier_id),
    FOREIGN KEY (supplier_id)
        REFERENCES suppliers(id)
        ON DELETE CASCADE
);

/* TABLES > SUPPLIERS > STUFF SUPPLIERS > STUFF PHOTOS */
CREATE TABLE suppliers_stuff_photos (
    id INT(11) PRIMARY KEY AUTO_INCREMENT,
    stuff_supplier_id INT(11),
    photo_id INT(11),
    created_at VARCHAR(20) NULL,
    updated_at VARCHAR(20) NULL,
    INDEX photos_id_supplier_stuff_photos (photo_id),
    FOREIGN KEY (photo_id)
        REFERENCES medias(id)
        ON DELETE CASCADE,
    INDEX stuff_id_supplier_stuff_photos (stuff_supplier_id),
    FOREIGN KEY (stuff_supplier_id)
        REFERENCES suppliers_stuff(id)
        ON DELETE CASCADE
);

/* TABLES > STORES */
CREATE TABLE stores (
    id INT(11) PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(255) NOT NULL,
    address TEXT NOT NULL,
    description TEXT NOT NULL,
    banner_id INT(11) DEFAULT 2,
    face_id INT(11) DEFAULT 1,
    author_id INT(11),
    created_at VARCHAR(20) NULL,
    updated_at VARCHAR(20) NULL,
    INDEX banner_id_store (banner_id),
    FOREIGN KEY (banner_id)
        REFERENCES medias(id)
        ON DELETE CASCADE,
    INDEX face_id_store (face_id),
    FOREIGN KEY (face_id)
        REFERENCES medias(id)
        ON DELETE CASCADE,
    INDEX author_id_store (author_id),
    FOREIGN KEY (author_id)
        REFERENCES users(id)
        ON DELETE CASCADE
);

/* TABLES > STORES > UTILITY */
CREATE TABLE stores_utility (
    id INT(11) PRIMARY KEY AUTO_INCREMENT,
    is_banned TINYINT(1) DEFAULT 0,
    balance BIGINT(20) DEFAULT 0,
    transaction BIGINT(20) DEFAULT 0,
    casier BIGINT(20) DEFAULT 0,
    stuff BIGINT(20) DEFAULT 0,
    store_id INT(11),
    INDEX store_id_utility (store_id),
    FOREIGN KEY (store_id)
        REFERENCES stores(id)
        ON DELETE CASCADE
);

/* TABLES > STORES > STUFF STORES */
CREATE TABLE stores_stuff (
    id INT(11) PRIMARY KEY AUTO_INCREMENT,
    label VARCHAR(255) NOT NULL,
    code VARCHAR(255) NOT NULL,
    description TEXT NOT NULL,
    qty BIGINT(20) DEFAULT 0,
    price_default BIGINT(20) DEFAULT 0,
    price_selling BIGINT(20) DEFAULT 0,
    expired_date VARCHAR(20) NULL,
    created_at VARCHAR(20) NULL,
    updated_at VARCHAR(20) NULL,
    store_id INT(11),
    stuff_supplier_id INT(11),
    INDEX stuff_supplier_id_stuff_store (stuff_supplier_id),
    FOREIGN KEY (stuff_supplier_id)
        REFERENCES suppliers_stuff(id)
        ON DELETE CASCADE,
    INDEX store_id_stuff_store (store_id),
    FOREIGN KEY (store_id)
        REFERENCES stores(id)
        ON DELETE CASCADE
);

/* TABLES > STORES > STUFF STORES > STUFF PHOTOS */
CREATE TABLE stores_stuff_photos (
    id INT(11) PRIMARY KEY AUTO_INCREMENT,
    stuff_store_id INT(11),
    photo_id INT(11),
    created_at VARCHAR(20) NULL,
    updated_at VARCHAR(20) NULL,
    INDEX photos_id_store_stuff_photos (photo_id),
    FOREIGN KEY (photo_id)
        REFERENCES medias(id)
        ON DELETE CASCADE,
    INDEX stuff_id_store_stuff_photos (stuff_store_id),
    FOREIGN KEY (stuff_store_id)
        REFERENCES stores_stuff(id)
        ON DELETE CASCADE
);

/* TABLES > ORDERS STORE TO SUPPLIER */
CREATE TABLE orders_store_to_supplier (
    id INT(11) PRIMARY KEY AUTO_INCREMENT,
    store_id INT(11),
    total_price BIGINT(20) NOT NULL DEFAULT 0,
    created_at VARCHAR(20) NULL,
    updated_at VARCHAR(20) NULL,
    INDEX store_id_order_store_to_supplier (store_id),
    FOREIGN KEY (store_id)
        REFERENCES stores(id)
        ON DELETE CASCADE
);

/* TABLES > ORDERS STORE TO SUPPLIER > DETAILS */
CREATE TABLE orders_store_to_supplier_detail (
    id INT(11) PRIMARY KEY AUTO_INCREMENT,
    order_store_to_supplier_id INT(11),
    stuff_supplier_id INT(11),
    qty BIGINT(20) NOT NULL DEFAULT 0,
    sub_total_price BIGINT(20) NOT NULL DEFAULT 0,
    status TINYINT(1) NOT NULL DEFAULT 0,
    created_at VARCHAR(20) NULL,
    updated_at VARCHAR(20) NULL,
    INDEX order_store_to_supplier_id_detail (order_store_to_supplier_id),
    FOREIGN KEY (order_store_to_supplier_id)
        REFERENCES orders_store_to_supplier(id)
        ON DELETE CASCADE,
    INDEX stuff_supplier_id_detail (stuff_supplier_id),
    FOREIGN KEY (stuff_supplier_id)
        REFERENCES suppliers_stuff(id)
        ON DELETE CASCADE
);

/* TABLES > ORDERS USER TO STORE */
CREATE TABLE orders_user_to_store (
    id INT(11) PRIMARY KEY AUTO_INCREMENT,
    user_id INT(11),
    total_price BIGINT(20) NOT NULL DEFAULT 0,
    created_at VARCHAR(20) NULL,
    updated_at VARCHAR(20) NULL,
    INDEX user_id_order_user_to_store (user_id),
    FOREIGN KEY (user_id)
        REFERENCES users(id)
        ON DELETE CASCADE
);

/* TABLES > ORDERS USER TO STORE */
CREATE TABLE orders_user_to_supplier (
    id INT(11) PRIMARY KEY AUTO_INCREMENT,
    user_id INT(11),
    total_price BIGINT(20) NOT NULL DEFAULT 0,
    created_at VARCHAR(20) NULL,
    updated_at VARCHAR(20) NULL,
    INDEX user_id_order_user_to_supplier (user_id),
    FOREIGN KEY (user_id)
        REFERENCES users(id)
        ON DELETE CASCADE
);

/* TABLES > ORDERS USERS TO STORES > DETAILS */
CREATE TABLE orders_user_to_store_detail (
    id INT(11) PRIMARY KEY AUTO_INCREMENT,
    order_user_to_store_id INT(11),
    stuff_store_id INT(11),
    qty BIGINT(20) NOT NULL DEFAULT 0,
    sub_total_price BIGINT(20) NOT NULL DEFAULT 0,
    status TINYINT(1) NOT NULL DEFAULT 0,
    created_at VARCHAR(20) NULL,
    updated_at VARCHAR(20) NULL,
    INDEX order_user_to_store_id_detail (order_user_to_store_id),
    FOREIGN KEY (order_user_to_store_id)
        REFERENCES orders_user_to_store(id)
        ON DELETE CASCADE,
    INDEX stuff_store_id_detail (stuff_store_id),
    FOREIGN KEY (stuff_store_id)
        REFERENCES stores_stuff(id)
        ON DELETE CASCADE
);

/* TABLES > ORDERS USERS TO STORES > DETAILS */
CREATE TABLE orders_user_to_supplier_detail (
    id INT(11) PRIMARY KEY AUTO_INCREMENT,
    order_user_to_supplier_id INT(11),
    stuff_supplier_id INT(11),
    qty BIGINT(20) NOT NULL DEFAULT 0,
    sub_total_price BIGINT(20) NOT NULL DEFAULT 0,
    status TINYINT(1) NOT NULL DEFAULT 0,
    created_at VARCHAR(20) NULL,
    updated_at VARCHAR(20) NULL,
    INDEX order_user_to_supplier_id_detail (order_user_to_supplier_id),
    FOREIGN KEY (order_user_to_supplier_id)
        REFERENCES orders_user_to_supplier(id)
        ON DELETE CASCADE,
    INDEX stuff_supplier_id_detail (stuff_supplier_id),
    FOREIGN KEY (stuff_supplier_id)
        REFERENCES suppliers_stuff(id)
        ON DELETE CASCADE
);

/* TABLES > CART STORES */
CREATE TABLE cart_store (
    id INT(11) PRIMARY KEY AUTO_INCREMENT,
    store_id INT(11),
    total_price BIGINT(20) NOT NULL DEFAULT 0,
    created_at VARCHAR(20) NULL,
    updated_at VARCHAR(20) NULL,
    INDEX store_id_cart_store (store_id),
    FOREIGN KEY (store_id)
        REFERENCES stores(id)
        ON DELETE CASCADE
);

/* TABLES > CART STORES > DETAILS */
CREATE TABLE cart_store_detail (
    id INT(11) PRIMARY KEY AUTO_INCREMENT,
    cart_store_id INT(11),
    stuff_supplier_id INT(11),
    qty BIGINT(20) NOT NULL DEFAULT 0,
    sub_total_price BIGINT(20) NOT NULL DEFAULT 0,
    status TINYINT(1) NOT NULL DEFAULT 0,
    created_at VARCHAR(20) NULL,
    updated_at VARCHAR(20) NULL,
    INDEX cart_store_id_detail (cart_store_id),
    FOREIGN KEY (cart_store_id)
        REFERENCES cart_store(id)
        ON DELETE CASCADE,
    INDEX stuff_supplier_id_detail (stuff_supplier_id),
    FOREIGN KEY (stuff_supplier_id)
        REFERENCES suppliers_stuff(id)
        ON DELETE CASCADE
);

/* TABLES > CART USERS */
CREATE TABLE cart_user (
    id INT(11) PRIMARY KEY AUTO_INCREMENT,
    user_id INT(11),
    total_price BIGINT(20) NOT NULL DEFAULT 0,
    created_at VARCHAR(20) NULL,
    updated_at VARCHAR(20) NULL,
    INDEX user_id_cart_user (user_id),
    FOREIGN KEY (user_id)
        REFERENCES users(id)
        ON DELETE CASCADE
);

/* TABLES > CART USERS > DETAILS */
CREATE TABLE cart_user_store_detail (
    id INT(11) PRIMARY KEY AUTO_INCREMENT,
    cart_user_id INT(11),
    stuff_store_id INT(11),
    qty BIGINT(20) NOT NULL DEFAULT 0,
    sub_total_price BIGINT(20) NOT NULL DEFAULT 0,
    status TINYINT(1) NOT NULL DEFAULT 0,
    created_at VARCHAR(20) NULL,
    updated_at VARCHAR(20) NULL,
    INDEX cart_user_id_detail (cart_user_id),
    FOREIGN KEY (cart_user_id)
        REFERENCES cart_user(id)
        ON DELETE CASCADE,
    INDEX stuff_store_id_detail (stuff_store_id),
    FOREIGN KEY (stuff_store_id)
        REFERENCES stores_stuff(id)
        ON DELETE CASCADE
);

/* TABLES > CART USERS > DETAILS */
CREATE TABLE cart_user_supplier_detail (
    id INT(11) PRIMARY KEY AUTO_INCREMENT,
    cart_user_id INT(11),
    stuff_supplier_id INT(11),
    qty BIGINT(20) NOT NULL DEFAULT 0,
    sub_total_price BIGINT(20) NOT NULL DEFAULT 0,
    status TINYINT(1) NOT NULL DEFAULT 0,
    created_at VARCHAR(20) NULL,
    updated_at VARCHAR(20) NULL,
    INDEX cart_user_supplier_id_detail (cart_user_id),
    FOREIGN KEY (cart_user_id)
        REFERENCES cart_user(id)
        ON DELETE CASCADE,
    INDEX stuff_supplier_id_detail (stuff_supplier_id),
    FOREIGN KEY (stuff_supplier_id)
        REFERENCES suppliers_stuff(id)
        ON DELETE CASCADE
);

/* TABLES > CASIER STORE */
CREATE TABLE casier_store (
    id INT(11) PRIMARY KEY AUTO_INCREMENT,
    store_id INT(11),
    total_price BIGINT(20) NOT NULL DEFAULT 0,
    created_at VARCHAR(20) NULL,
    updated_at VARCHAR(20) NULL,
    INDEX store_id_casier_store (store_id),
    FOREIGN KEY (store_id)
        REFERENCES stores(id)
        ON DELETE CASCADE
);

/* TABLES > CART USERS > DETAILS */
CREATE TABLE casier_store_detail (
    id INT(11) PRIMARY KEY AUTO_INCREMENT,
    casier_store_id INT(11),
    stuff_store_id INT(11),
    qty BIGINT(20) NOT NULL DEFAULT 0,
    sub_total_price BIGINT(20) NOT NULL DEFAULT 0,
    status TINYINT(1) NOT NULL DEFAULT 0,
    created_at VARCHAR(20) NULL,
    updated_at VARCHAR(20) NULL,
    INDEX cart_store_id_casier_detail (casier_store_id),
    FOREIGN KEY (casier_store_id)
        REFERENCES casier_store(id)
        ON DELETE CASCADE,
    INDEX stuff_store_id_casier_detail (stuff_store_id),
    FOREIGN KEY (stuff_store_id)
        REFERENCES stores_stuff(id)
        ON DELETE CASCADE
);

/* TABLES > CASIER SUPPLIER */
CREATE TABLE casier_supplier (
    id INT(11) PRIMARY KEY AUTO_INCREMENT,
    supplier_id INT(11),
    total_price BIGINT(20) NOT NULL DEFAULT 0,
    created_at VARCHAR(20) NULL,
    updated_at VARCHAR(20) NULL,
    INDEX supplier_id_casier_supplier (supplier_id),
    FOREIGN KEY (supplier_id)
        REFERENCES suppliers(id)
        ON DELETE CASCADE
);

/* TABLES > CASIER SUPPLIER > DETAILS */
CREATE TABLE casier_supplier_detail (
    id INT(11) PRIMARY KEY AUTO_INCREMENT,
    casier_supplier_id INT(11),
    stuff_supplier_id INT(11),
    qty BIGINT(20) NOT NULL DEFAULT 0,
    sub_total_price BIGINT(20) NOT NULL DEFAULT 0,
    status TINYINT(1) NOT NULL DEFAULT 0,
    created_at VARCHAR(20) NULL,
    updated_at VARCHAR(20) NULL,
    INDEX cart_supplier_id_casier_detail (casier_supplier_id),
    FOREIGN KEY (casier_supplier_id)
        REFERENCES casier_supplier(id)
        ON DELETE CASCADE,
    INDEX stuff_supplier_id_casier_detail (stuff_supplier_id),
    FOREIGN KEY (stuff_supplier_id)
        REFERENCES suppliers_stuff(id)
        ON DELETE CASCADE
);

/* TABLES > BASE CHAT */
CREATE TABLE base_chat (
    id INT(11) PRIMARY KEY AUTO_INCREMENT,
    chat_type TINYINT(1) DEFAULT 0,
    reference VARCHAR(255) NOT NULL,
    created_at VARCHAR(20) NULL,
    updated_at VARCHAR(20) NULL
);

/* TABLES > SUPPLIER STUFF > TAGS */
CREATE TABLE suppliers_stuff_tags (
    id INT(11) PRIMARY KEY AUTO_INCREMENT,
    stuff_supplier_id INT(11),
    tag VARCHAR(255) NOT NULL,
    INDEX stuff_supplier_id_tags (stuff_supplier_id),
    FOREIGN KEY (stuff_supplier_id)
        REFERENCES suppliers_stuff(id)
        ON DELETE CASCADE
);

/* TABLES > STORES STUFF > TAGS */
CREATE TABLE stores_stuff_tags (
    id INT(11) PRIMARY KEY AUTO_INCREMENT,
    stuff_store_id INT(11),
    tag VARCHAR(255) NOT NULL,
    INDEX stuff_store_id_tags (stuff_store_id),
    FOREIGN KEY (stuff_store_id)
        REFERENCES stores_stuff(id)
        ON DELETE CASCADE
);

/* TABLES > STORES STUFF > LOGS */
CREATE TABLE stores_stuff_logs (
    id INT(11) PRIMARY KEY AUTO_INCREMENT,
    store_stuff_id INT(11),
    user_id INT(11),
    log_type TINYINT(1),
    created_at VARCHAR(20) NULL,
    updated_at VARCHAR(20) NULL,
    INDEX stuff_store_id_store_logs (store_stuff_id),
    FOREIGN KEY (store_stuff_id)
        REFERENCES stores_stuff(id)
        ON DELETE CASCADE,
    INDEX user_id_store_logs (user_id),
    FOREIGN KEY (user_id)
        REFERENCES users(id)
        ON DELETE CASCADE
);

/* TABLES > STORES STUFF > LOGS */
CREATE TABLE suppliers_stuff_logs (
    id INT(11) PRIMARY KEY AUTO_INCREMENT,
    supplier_stuff_id INT(11),
    user_id INT(11),
    log_type TINYINT(1),
    created_at VARCHAR(20) NULL,
    updated_at VARCHAR(20) NULL,
    INDEX stuff_supplier_id_supplier_logs (supplier_stuff_id),
    FOREIGN KEY (supplier_stuff_id)
        REFERENCES suppliers_stuff(id)
        ON DELETE CASCADE,
    INDEX user_id_supplier_logs (user_id),
    FOREIGN KEY (user_id)
        REFERENCES users(id)
        ON DELETE CASCADE
);

/* TABLES > STORES STUFF > COMMENTS */
CREATE TABLE suppliers_stuff_comments (
    id INT(11) PRIMARY KEY AUTO_INCREMENT,
    stuff_supplier_id INT(11),
    user_id INT(11),
    parent_id INT(11) DEFAULT 0,
    comment TEXT NOT NULL,
    created_at VARCHAR(20) NULL,
    updated_at VARCHAR(20) NULL,
    INDEX stuff_supplier_id_supplier_stuff_comment (stuff_supplier_id),
    FOREIGN KEY (stuff_supplier_id)
        REFERENCES suppliers_stuff(id)
        ON DELETE CASCADE,
    INDEX user_id_supplier_stuff_comment (user_id),
    FOREIGN KEY (user_id)
        REFERENCES users(id)
        ON DELETE CASCADE
);

/* TABLES > STORES STUFF > COMMENTS */
CREATE TABLE stores_stuff_comments (
    id INT(11) PRIMARY KEY AUTO_INCREMENT,
    stuff_store_id INT(11),
    user_id INT(11),
    parent_id INT(11) DEFAULT 0,
    comment TEXT NOT NULL,
    created_at VARCHAR(20) NULL,
    updated_at VARCHAR(20) NULL,
    INDEX stuff_store_id_store_stuff_comment (stuff_store_id),
    FOREIGN KEY (stuff_store_id)
        REFERENCES stores_stuff(id)
        ON DELETE CASCADE,
    INDEX user_id_store_stuff_comment (user_id),
    FOREIGN KEY (user_id)
        REFERENCES users(id)
        ON DELETE CASCADE
);

CREATE TABLE top_up_info (
    id INT(11) PRIMARY KEY AUTO_INCREMENT,
    bank_name VARCHAR(255) NOT NULL,
    account_number VARCHAR(255) NOT NULL,
    steps TEXT NOT NULL,
    created_at VARCHAR(20) NULL,
    updated_at VARCHAR(20) NULL
);

/* TABLES > USER UTILITY > TOP UP */
CREATE TABLE top_up_users (
    id INT(11) PRIMARY KEY AUTO_INCREMENT,
    user_utility_id INT(11),
    req_balance BIGINT(20) DEFAULT 0,
    top_up_info_id INT(11),
    user_bank_name VARCHAR(255) NOT NULL,
    user_account_number VARCHAR(255) NOT NULL,
    status TINYINT(1) NOT NULL DEFAULT 0,
    created_at VARCHAR(20) NULL,
    updated_at VARCHAR(20) NULL,
    INDEX user_utility_top_up (user_utility_id),
    FOREIGN KEY (user_utility_id)
        REFERENCES users_utility(id)
        ON DELETE CASCADE,
    INDEX info_top_up (top_up_info_id),
    FOREIGN KEY (top_up_info_id)
        REFERENCES top_up_info(id)
        ON DELETE CASCADE
);

/* TABLES > USER UTILITY > TOP UP > PROOF */
CREATE TABLE top_up_users_proof (
    id INT(11) PRIMARY KEY AUTO_INCREMENT,
    top_up_user_id INT(11),
    proof_id INT(11),
    created_at VARCHAR(20) NULL,
    updated_at VARCHAR(20) NULL,
    INDEX top_up_user_id_proof (top_up_user_id),
    FOREIGN KEY (top_up_user_id)
        REFERENCES top_up_users(id)
        ON DELETE CASCADE,
    INDEX proof_id_proof (proof_id),
    FOREIGN KEY (proof_id)
        REFERENCES medias(id)
        ON DELETE CASCADE
);